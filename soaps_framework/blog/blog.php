<?php
global $routes, $actions;
/* This is the file that controls the blog-like functionality of SOAPS.
   This is incredibly minimalistic, as its only goal is to serve client-generated
   web pages at a moderate speed. This is not meant for high-traffic sites,
   it is meant for small businesses, etc. that will only see a couple hundred hits a day.

   That said, this system is _really_ flexible, and you can do almost anything to it.

   SETUP:
	 Set `BLOG_ENABLED` to `true` in config.php
	 `BLOG_PATH` is the path that will be prepended to all blog URLs.
	 `BLOG_TPL` is the name of the template (in project_directory/templates) that will
	   be used to serve all blog posts.
	 Navigate to http://server/`BLOG_PATH`/config and follow the on-screen instructions
	 To work with the blog, go to http://server/`BLOG_PATH`/admin
   
   WRITING A TEMPLATE:
	 BLOG_IS_POST is a boolean determining if the page is a blog post or a listing of pages
	 BLOG_LIST is an array of blog posts                                      [BLOG_IS_POST = false]
	 BLOG_PAGE_NAME is the title of the current post                          [BLOG_IS_POST = true]
	 BLOG_PAGE_AUTHOR is the name of the author of the current post           [BLOG_IS_POST = true]
	 BLOG_PAGE_CATEGORIES is an array of categories for the current post      [BLOG_IS_POST = true]

   CONFIGURATION:
*/
define('BLOG_TPL', "blog.php");

$blog_is_setup = file_exists(dirname(__FILE__) . "/config.json");

// Setting up routes
if(!$blog_is_setup) {
	$routes[BLOG_PATH . "/config"]   = [BLOG_TPL_DIR . "/init.php",[]];
	$actions[BLOG_PATH . "/admin/api/setup"] = [BLOG_TPL_DIR . "/init_action.php",[]];
} else {
	$routes[BLOG_PATH . "/post/:id"] = [BLOG_TPL];
	$routes[BLOG_PATH . "/admin"] = [BLOG_TPL_DIR . "/admin_tpl.php", ["page" => "index"]];
	$routes[BLOG_PATH . "/admin/users"] = [BLOG_TPL_DIR . "/admin_tpl.php",["page" => "users"]];
	$actions[BLOG_PATH . "/admin/api/user/:id/delete"] = [BLOG_TPL_DIR . "/admin_action.php",["action" => "delete_user"]];
	$actions[BLOG_PATH . "/admin/api/user/create"] = [BLOG_TPL_DIR . "/admin_action.php",["action" => "create_user"]];
	$routes[BLOG_PATH . "/admin/posts"] = [BLOG_TPL_DIR . "/admin_tpl.php",["page" => "posts"]];
	$routes[BLOG_PATH . "/admin/posts/:id/edit"] = [BLOG_TPL_DIR . "/admin_tpl.php",["page" => "edit"]];
	$actions[BLOG_PATH . "/admin/api/post/:id/delete"] = [BLOG_TPL_DIR . "/admin_action.php",["action" => "delete_post"]];
	$actions[BLOG_PATH . "/admin/api/post/:id/update"] = [BLOG_TPL_DIR . "/admin_action.php",["action" => "update_post"]];
	$actions[BLOG_PATH . "/admin/api/post/create"] = [BLOG_TPL_DIR . "/admin_action.php",["action" => "create_post"]];
	$routes[BLOG_PATH . "/admin/login"] = [BLOG_TPL_DIR . "/admin_tpl.php",["page" => "login"]];
	$actions[BLOG_PATH . "/admin/api/logout"] = [BLOG_TPL_DIR . "/admin_action.php",["action" => "logout"]];
	$actions[BLOG_PATH . "/admin/api/login"] = [BLOG_TPL_DIR . "/admin_action.php",["action" => "login"]];
}

$pdo = null;
// Setting up connections and loading vars
// This assumes that $PAGE_ARGS is already set.
function blog_page_init() {
	global $blog_is_setup, $pdo;

	if(!$blog_is_setup) die("Please configure your blog. <a href='" . BLOG_PATH . "/config'>Configuration</a>");

	$config = json_decode(file_get_contents(dirname(__FILE__) . "/config.json"), true);
	try {
		$pdo = new PDO(
			"mysql:host=" . $config['hn'] . ";dbname=" . $config['db'] . ";charset=utf8",
			$config['un'], $config['pw'],
			[
				PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
				PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
				PDO::ATTR_EMULATE_PREPARES   => false,
			]
		);
	} catch (PDOException $e) {
		die("Failed to connect to the database.");
	}
}

$session_name = 'soaps_user_session';

function add_user($pdo, $un, $pass, $real_name) {

}

function sec_session_start() {

	session_name($session_name);

	$secure = true;
	// This stops JS being able to access the session id.
	$httponly = true;
	// Forces sessions to only use cookies.
	if (ini_set('session.use_only_cookies', 1) === FALSE) {
		// header("Location: ../error.php?err=Could not initiate a safe session (ini_set)");
		die("Failed to start a safe session.");
		exit();
	}
	// Gets current cookies params.
	$cookie_params = session_get_cookie_params();
	session_set_cookie_params($cookie_params["lifetime"],
		$cookie_params["path"], 
		$cookie_params["domain"], 
		$secure,
		$httponly);

	session_start(); // Start the PHP session 
	session_regenerate_id(true); // regenerated the session, delete the old one. 
}

function checkbrute($id) {
	global $pdo;

    // Get timestamp of current time 
    $now = time();
 
    // All login attempts are counted from the past 2 hours. 
    $valid_attempts = $now - (2 * 60 * 60);
 
    if ($stmt = $pdo->prepare("SELECT time 
                               FROM attempts 
                               WHERE user_id = ? 
                               AND time > '$valid_attempts'")) {
 
        // Execute the prepared query. 
        $stmt->execute([$id]);
 
        // If there have been more than 5 failed logins 
        if ($stmt->rowCount() > 5) {
            return true;
        } else {
            return false;
        }
    }
}

function login($username, $password) {
	global $pdo;

	if ($stmt = $pdo->prepare("SELECT id, password 
		FROM users
		WHERE username = ?
		LIMIT 1")) {
		$stmt->execute([$username]);
		if($stmt->rowCount() > 1) {

			$row = $stmt[0];

			$data = $stmt->fetch(PDO::FETCH_ASSOC);

        	$password = hash('sha512', $password . $row["salt"] . PARTIAL_PASS_SALT);

			if (checkbrute($data["id"]) == true) {
				// Account is locked 
				// TODO(zachary): Should I do something?
				return false;
			} else {
				// Check if the password in the database matches
				// the password the user submitted. We are using
				// the password_verify function to avoid timing attacks.
				if ($password == $row["password"]) {
					// Password is correct!
					// Get the user-agent string of the user.
					$user_browser = $_SERVER['HTTP_USER_AGENT'];
					// XSS protection as we might print this value
					$user_id = preg_replace("/[^0-9]+/", "", $data["id"]);
					$_SESSION['user_id'] = $user_id;
					// XSS protection as we might print this value
					$username = preg_replace("/[^a-zA-Z0-9_\-]+/", 
																"", 
																$data["username"]);
					$_SESSION['username'] = $username;
					$_SESSION['login_string'] = hash('sha512', 
							  $db_password . $user_browser);
					// Login successful.
					return true;
				} else {
					// Password is not correct
					// We record this attempt in the database
					$now = time();
					$mysqli->query("INSERT INTO login_attempts(user_id, time)
									VALUES ('$user_id', '$now')");
					return false;
				}
			}
		} else {
			// No user exists.
			return false;
		}
	}
}