<?php
$font = "Roboto, Helvetica Neue, Open Sans, Cantarell, Segoe UI Light, Segoe UI, sans-serif";
$bg = "#f9f9ff";
$text_color = "#223";
$shadow_color = "rgba(0,0,0,0.15)";
$CSS = <<<ENDCSS
@import url('https://fonts.googleapis.com/css?family=Roboto:300,400,900');
* {
	font-family: $font;
}

.admin-page {
	background: $bg;
	color: $text_color;
}

.section-header {
	font-weight: 100;
	font-size: 1.5em;
}

.admin-form {
	background-color: #eee;
}

.form-conditional {
	display: block;
	background-color: #e4e4e4;
	padding: .5em;
	box-shadow: inset 0 14px 12px -12px $shadow_color, inset 0 -14px 12px -12px $shadow_color;
}

.form-element {
	padding: .2em;
}
.form-element > label {
	width: 12em;
	display: inline-block;
}

.form-element > label.checkbox-click-zone {
	width: 1.5em;
    height: 1.5em;
    position: absolute;
    margin-top: -3px;
    margin-left: -2px;

}
.password-options > * {
	margin: 0 .5em;
	display: inline-block;
}
.password-options > button {
	margin-left: 1em;
}
.password-options > input {
	margin-left: 0;
}

ENDCSS;
echo $CSS;

function section_header($name) {
	echo "<span class=\"section-header\">$name</span>";
}
function form_elem($name, $type, $id, $default=null, $check_mode="") {
	if($default != null) {
		if($type == "checkbox") {
			if($default) $default = "checked=\"1\"";
		} else {
			$default = "value=\"$default\"";
		}
	} else {$default="";}
	$checkbox_click_zone = "";
	if($type == "checkbox") {
		$checkbox_click_zone = "<label for=\"$id\" class=\"checkbox-click-zone\"></label>";
	}
	$required = "";
	if($check_mode != "") {
		$required = "<span class=\"required-mark\">*</span>";
	}
	$password_extras = "";
	if($check_mode == "password") {
		$password_extras = "<span class=\"password-options\" data-target=\"#$id\">" .
		"<button class=\"random-password-btn\">Generate Random Password</button>" .
		"<label for=\"show-$id\">Show Password?</label>" .
		"<input type=\"checkbox\" id=\"show-$id\">" .
		"</span>";
	}
	echo "<div class=\"form-element\"><label for=\"$id\">$name $required</label>$checkbox_click_zone<input data-check=\"$check_mode\" type=\"$type\" name=\"$id\" id=\"$id\" class=\"checked-field\" $default placeholder=\"$name\">$password_extras</div>";
}