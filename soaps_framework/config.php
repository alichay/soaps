<?php
define('SITE_DIR', __DIR__."/..");
define('REWRITE_ENABLED', array_key_exists('HTTP_MOD_REWRITE', $_SERVER));
define('WWW_DIR', SITE_DIR . "/www");

// NOTE: If you change TPL_DIR, make sure that BLOG_TPL_DIR is still a valid route.
define('TPL_DIR', SITE_DIR . "/soaps_website");
define('BLOG_TPL_DIR', "../../soaps_framework/blog");

// See the top of "blog/blog.php" for information about this
define('BLOG_ENABLED', true);
// This is prepended to every blog-related URL.
// E.g. if this is set to "/blog" then a post will be located at
//      http://mysite.com/blog/post/$POST_ID
define('BLOG_PATH', "/news");

// Password hashing is a multi-step process.
// The password, before it is hashed, has two strings appended to it.
// The first one is a user-specific hash, and the second is a site-specific hash.
// The goal is to make it as difficult as possible to crack, because hash-cracking is not
// technically possible, but it is severely limited by the complexity of the task.
// The more places where we split up the information required to break the pass,
// the more theoretically secure out site is.

// CHANGE THIS!
// Just slam the keyboard a couple times to make a pseudo-random string.
define('PARTIAL_PASS_SALT', '2HLJSUd9ipsajuod:8disaj34324');