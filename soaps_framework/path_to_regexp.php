<?php
function parse_path($path) {

	$ret = [
		"args" => []
	];
	$path = rtrim(ltrim($path, '/'), '/').'/';
	$arr = str_split($path);
	$is_argument = false;
	$string_builder = '';
	$regex = "/^";

	for($i=0;$i<count($arr);$i++) {
		$c = $arr[$i];
		if($c=='/') {

			if($is_argument) {
				$regex .= "\\/(.+?)";
				$ret["args"][] = $string_builder;
			} else {
				$regex .= "\\/" . $string_builder;
			}

			$is_argument = false;
			$string_builder = '';
		} elseif($c==':' && strlen($string_builder)==0) {
			$is_argument = true;
		} else {
			$string_builder .= $c;
		}
	}

	$regex .= "(?:\\/$|$)/";

	$ret["regex"] = $regex;
	return $ret;
}
