// Load the SOAPS JS module.
import * as soaps_module from "soaps_module.js";

// Your code goes here.
// You might want to modularize it, and just
// load the modules here to keep things simple.
soaps_module.addEventListener('page_change', ()=>{
	
	let navbar = document.querySelector("header>nav");

	let previous = navbar.getElementsByClassName("active")[0];
	let next = navbar.querySelector(`a[href='${location.pathname}']`);
	
	if(previous) previous.classList.remove("active");
	if(next) next.classList.add("active");
});

// Initialize the SOAPS module after all user-defined events are registered.
// This is done so that events added to `page_change` can be executed on first load
// as well as subsequent requests handled with AJAX.
soaps_module.init();