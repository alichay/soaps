#!/bin/env node
const fs = require('fs');
const path = require('path');
const babel = require('babel-core');
const watch = require('node-watch');
const stylus = require('stylus');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const postcss = require('postcss');
const webpack = require("webpack");

const js_file = 'site.js';
const js_temporary_file = 'compiling_js.tmp';
const stylus_file = 'site.styl';

function compiler(type, process_step) {
	let writing = false;
	let count = 0;
	return () => {
		if(writing) return;
		writing = true;
		count++;
		console.log(`Compiling   ${type}: ${count}`);
		process_step(()=>{
			console.log(`Finished    ${type}: ${count}`);
			writing = false;
		})
	}
}

const compile_js = compiler("Javascript", (cb)=>{
	webpack({
		entry: path.resolve(__dirname, 'js/'+js_file),
		output: {
			path: path.resolve(__dirname, '../www/js'),
			filename: js_file
		},
		module: {
			rules: [
				{
					test: /\.jsx?$/,
					loader: 'babel-loader',
					options: {
						presets: [
							'babili', 'env', 'react'
						]
					}
				}
			]
		},
		resolve: {
			modules: [
				'js/node_modules',
				'js'
			],
			extensions: [".js", ".jsx"],
		},
		target: "web"
	}, (err, stats) => {
		if (err || stats.hasErrors()) {
			console.error(err);
			process.stdout.write(stats.toString({colors: true})+"\n");
			cb();
		}
		cb();
	})
});

watch('js', compile_js);

const compile_styl = compiler("Stylesheet", (cb)=>{
	stylus(fs.readFileSync('styl/'+stylus_file, 'utf8')).set('filename', 'styl/'+stylus_file).render((err, css)=>{
		if(err) {
			console.error(err);
			cb();
		} else {
			postcss([ autoprefixer, cssnano ]).process(css).then(function (res) {
				res.warnings().forEach(function (warn) {
					console.warn(warn.toString());
				});
				fs.writeFile(`../www/css/${stylus_file.replace('styl', 'css')}`, res.css, cb);
			});
		}
	});
});

watch('styl', compile_styl);



compile_js();
compile_styl();
